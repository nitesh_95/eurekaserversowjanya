package com.bhushan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SpringTestProject21Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringTestProject21Application.class, args);
	}

}
